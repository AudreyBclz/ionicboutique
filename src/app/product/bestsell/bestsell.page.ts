import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/model/product';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-bestsell',
  templateUrl: './bestsell.page.html',
  styleUrls: ['./bestsell.page.scss'],
})
export class BestsellPage implements OnInit {

  result: any;
  products: Product[];
  constructor(
    private router: Router,
    private productService: ProductService
  ) { }

  ngOnInit() {
    this.getBestSellProd();
  }

  goDetail(id: number){
    this.router.navigate(['product','detail',id]);
  }

  async getBestSellProd(){
    this.productService.findBestSellProd().subscribe(prod =>{
      this.result =prod;
      this.products = this.result.data;
    });
  };
}
