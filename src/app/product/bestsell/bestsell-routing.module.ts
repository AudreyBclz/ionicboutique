import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { BestsellPage } from './bestsell.page';

const routes: Routes = [
  {
    path: '',
    component: BestsellPage
  }
];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class BestsellPageRoutingModule {}
