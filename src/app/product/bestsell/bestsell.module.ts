import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { BestsellPageRoutingModule } from './bestsell-routing.module';

import { BestsellPage } from './bestsell.page';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    BestsellPageRoutingModule
  ],
  declarations: [BestsellPage]
})
export class BestsellPageModule {}
