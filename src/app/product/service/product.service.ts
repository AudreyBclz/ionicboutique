import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Product } from 'src/app/model/product';

@Injectable({
  providedIn: 'root'
})
export class ProductService {

  url='http://localhost:4000/api/boutique/products';
  constructor(private http: HttpClient) { }

  findfeaturedProd(){
    return this.http.get<Product[]>(this.url+'/find/featured');
  }
  findBestSellProd(){
    return this.http.get<Product[]>(this.url+'/find/bestsell');
  }
  findAProduct(id: number){
    return this.http.get<Product>(this.url+'/'+id);
  }
  findProductsByCat(id: number){
    return this.http.get<Product[]>(this.url+'/sortbycategorie/'+id);
  }
}
