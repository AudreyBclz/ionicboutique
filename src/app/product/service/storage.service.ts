/* eslint-disable no-underscore-dangle */
import { Injectable } from '@angular/core';
import { Storage } from '@ionic/storage';


@Injectable({
  providedIn: 'root'
})
export class StorageService {
 private _storage: Storage | null = null;

 constructor(private storage: Storage) {
   this.init();
 }

 async init() {
   // If using, define drivers here: await this.storage.defineDriver(/*...*/);
   const storage = await this.storage.create();
   this._storage = storage;
 }

 // Create and expose methods that users of this service can
 // call, for example:
 public set(key: string, value: any) {
   this._storage?.set(key, value);
 }

 public get(name: string): Promise<any[]>{
   return this._storage?.get(name);
 }

 async clear(name: string){
  await this._storage.remove(name);
 }
 async clearAll(){
  await this._storage.clear();
 }


}
