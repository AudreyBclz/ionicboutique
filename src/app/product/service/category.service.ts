import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Category } from 'src/app/model/category';

@Injectable({
  providedIn: 'root'
})
export class CategoryService {

  url='http://localhost:4000/api/boutique/category';
  constructor(private http: HttpClient) { }

  getAllCat(){
    return this.http.get<Category[]>(this.url);
  }
  getACat(id: number){
    return this.http.get<Category>(this.url+'/'+id);
  }
}
