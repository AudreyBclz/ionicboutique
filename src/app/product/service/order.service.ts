import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { Order } from 'src/app/model/order';
import { OrderLine } from 'src/app/model/order-line';

@Injectable({
  providedIn: 'root'
})
export class OrderService {

  url1 = 'http://localhost:4000/api/boutique/order';
  url2 = 'http://localhost:4000/api/boutique/order/orderline';

  constructor(private http: HttpClient) { }

  createOrder(order: Order){
    return this.http.post<Order>(this.url1,order);
  }

  createOrderLine(orderLine: OrderLine){
    return this.http.post<OrderLine>(this.url2,orderLine);
  }

  findOrderById(id: number){
    return this.http.get<Order>(this.url1);
  }

  findOrdersByIdUser(idUser: number){
    return this.http.get<Order[]>(this.url1+'/user/'+idUser);
  }

  findOrderLinesByIdOrder(idOrder: number){
    return this.http.get<Order[]>(this.url2+'/'+idOrder);
  }
}
