/* eslint-disable @typescript-eslint/type-annotation-spacing */
import { Component, OnInit } from '@angular/core';
import { Category } from '../model/category';
import { Product } from '../model/product';
import { NavService } from '../service/nav.service';
import { CategoryService } from './service/category.service';
import { ProductService } from './service/product.service';

@Component({
  selector: 'app-product',
  templateUrl: './product.page.html',
  styleUrls: ['./product.page.scss'],
})
export class ProductPage implements OnInit {

  displayNav=true;
  resultcat:any;
  resultFeat:any;
  resultBest:any;
  categories: Category[];
  prodFeatured:Product[];
  prodBestSell:Product[];
  constructor(
    private categoryService: CategoryService,
    private productService: ProductService,
    private navService: NavService) { }

  ngOnInit() {
    this.getCategories();
    this.getFeaturedProd();
    this.getBestSellProd();
    this.navService.changebool(this.displayNav);
  }

  // fonction pour récupérer les catégories
  async getCategories(){
    this.categoryService.getAllCat().subscribe(cat =>{
      this.resultcat=cat;
      this.categories = this.resultcat.data;
    });
  }

  // fonction pour récupérér les Features prod
  async getFeaturedProd(){
    this.productService.findfeaturedProd().subscribe(prod =>{
      this.resultFeat = prod;
      this.prodFeatured = this.resultFeat.data;
    });
  }

  // fonction pour récupérér les Best Seller
  async getBestSellProd(){
    this.productService.findBestSellProd().subscribe(prod =>{
      this.resultBest =prod;
      this.prodBestSell = this.resultBest.data;
    });
  };
}
