import { Component, OnInit } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Category } from 'src/app/model/category';
import { Product } from 'src/app/model/product';
import { CategoryService } from '../service/category.service';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-category',
  templateUrl: './category.page.html',
  styleUrls: ['./category.page.scss'],
})
export class CategoryPage implements OnInit {

  result: any;
  category: Category;
  products: Product[];
  id: number;

  constructor(
    private activeRoute: ActivatedRoute,
    private categoryService: CategoryService,
    private productService: ProductService,
    private router: Router
  ) { }

  ngOnInit() {
    this.id = this.activeRoute.snapshot.params.id;
    this.getOneCat(this.id);
    this.getProdByCat(this.id);

  }

  getOneCat(id: number){
    this.categoryService.getACat(id).subscribe(res =>{
      this.result = res;
      this.category = this.result.data;
    });
  }

  getProdByCat(id: number){
    this.productService.findProductsByCat(id).subscribe(res =>{
      this.result = res;
      this.products = this.result.data;
    });
  }

  goDetail(id: number){
    this.router.navigate(['product','detail',id]);
   }

}
