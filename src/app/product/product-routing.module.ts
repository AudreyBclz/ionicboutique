import { NgModule } from '@angular/core';
import { Routes, RouterModule } from '@angular/router';

import { ProductPage } from './product.page';

const routes: Routes = [
  {
    path: '',
    children: [
      {
      path:'',
      component:ProductPage,
    },
    {
      path: 'detail/:id',
      loadChildren: () => import('./detail-product/detail-product.module').then( m => m.DetailProductPageModule)
    },
    {
      path: 'category/:id',
      loadChildren: () => import('./category/category.module').then( m => m.CategoryPageModule)
    }
  ]},
  {
    path: 'categories',
    loadChildren: () => import('./categories/categories.module').then( m => m.CategoriesPageModule)
  },
  {
    path: 'featured',
    loadChildren: () => import('./featured/featured.module').then( m => m.FeaturedPageModule)
  },
  {
    path: 'bestsell',
    loadChildren: () => import('./bestsell/bestsell.module').then( m => m.BestsellPageModule)
  },
  {
    path: 'cart',
    loadChildren: () => import('./cart/cart.module').then( m => m.CartPageModule)
  },
  {
    path: 'order',
    loadChildren: () => import('./order/order.module').then( m => m.OrderPageModule)
  },
  {
    path: 'history-order',
    loadChildren: () => import('./history-order/history-order.module').then( m => m.HistoryOrderPageModule)
  },

];

@NgModule({
  imports: [RouterModule.forChild(routes)],
  exports: [RouterModule],
})
export class ProductPageRoutingModule {}
