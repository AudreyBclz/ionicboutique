/* eslint-disable max-len */
/* eslint-disable @typescript-eslint/prefer-for-of */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { Cart } from 'src/app/model/cart';
import { Product } from 'src/app/model/product';
import { User } from 'src/app/model/user';
import { NavService } from 'src/app/service/nav.service';
import { UserService } from 'src/app/service/user.service';
import { OrderService } from '../service/order.service';
import { ProductService } from '../service/product.service';
import { StorageService } from '../service/storage.service';

@Component({
  selector: 'app-order',
  templateUrl: './order.page.html',
  styleUrls: ['./order.page.scss'],
})
export class OrderPage implements OnInit {

  user: User;
  id: number = JSON.parse(localStorage.getItem('isLog')).id;
  idComande: number;
  product: Product;
  result: any;
  tabProduct: Product[];
  tabCart: Cart[];
  displayNav= true;
  constructor(
    private storageService: StorageService,
    private navside: NavService,
    private productService: ProductService,
    private userService: UserService,
    private router: Router,
    private toastController: ToastController,
    private orderService: OrderService) { }

  async ngOnInit() {
    this.navside.changebool(this.displayNav);
    await this.storageService.init();
    this.tabProduct=[];
    this.userService.findAUser(this.id).subscribe( user =>{
      this.result = user;
      this.user = this.result.data;
    });
  }
  async ionViewWillEnter(){
    this.tabProduct=[];
    this.storageService.get('cart').then(cart =>{
        this.tabCart = cart;
     }).then(() =>{
      this.tabCart.forEach(cartItem =>{
        this.findAProduct(cartItem.idProduct);
        console.log(this.tabProduct);
      });
     });
  }

  async findAProduct(id: number){
    this.productService.findAProduct(id).subscribe({next: (res) =>{
     this.result = res;
     this.product= this.result.data;
     this.tabProduct = [...this.tabProduct,this.product];
     console.log(this.tabProduct);

    }
   });
 }

 async clearAprod(id: number){
  this.tabCart.splice(id,1);
  this.storageService.set('cart',this.tabCart);
  this.storageService.get('cart').then(cart =>{
    this.tabCart = cart;
 }).then(() =>{
  this.tabProduct=[];
   this.tabCart.forEach(async cartItem =>{
      await this.findAProduct(cartItem.idProduct);
    console.log(this.tabProduct);
  });
 });
 const toast = await this.toastController.create({
  message: 'Le produit a bien été supprimé de votre panier',
  duration: 1500,
  position: 'top'
  });
  await toast.present();
  this.router.navigate(['product','detail',this.product.id]);
 }

 removeAQuantity(i: number){
  const quant = this.tabCart[i].quantity;
  if(quant>1){
    this.tabCart[i].quantity = this.tabCart[i].quantity-1;
  }else if(quant===1){
    this.clearAprod(i);
  }
  console.log(this.tabCart);

  this.storageService.set('cart',this.tabCart);
 }
 addAQuantity(i: number){
    this.tabCart[i].quantity = this.tabCart[i].quantity+1;
    this.storageService.set('cart',this.tabCart);
    console.log(this.tabCart);
 }

 calculSubTotal(): number{
  let subtotal=0;
  this.tabCart.forEach(c => {
   subtotal+= c.price*c.quantity;
  });
  return subtotal;
 }

 order(){
  this.orderService.createOrder({idUser:this.id}).subscribe(data =>{
    this.result = data;
    this.idComande = this.result.data.id;
    console.log(this.idComande);
    for(let i = 0; i<this.tabCart.length;i++){
      this.orderService.createOrderLine({
        nameProduct: this.tabProduct[i].name,
        quantity:this.tabCart[i].quantity,
        taille:this.tabCart[i].taille,
        color: this.tabCart[i].color,
        priceUnit : this.tabCart[i].price,
        idOrder:this.idComande}).subscribe();
   }
    this.tabCart=[];
    this.storageService.clear('cart');
    this.router.navigate(['','product','order','confirm']);
  });

 }


}
