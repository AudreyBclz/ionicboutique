import { Component, OnInit } from '@angular/core';
import { Category } from 'src/app/model/category';
import { NavService } from 'src/app/service/nav.service';
import { CategoryService } from '../service/category.service';

@Component({
  selector: 'app-categories',
  templateUrl: './categories.page.html',
  styleUrls: ['./categories.page.scss'],
})
export class CategoriesPage implements OnInit {

  displayNav=true;
  result: any;
  categories: Category[];
  constructor(
    private categoryService: CategoryService,
    private navService: NavService) { }

  ngOnInit() {
    this.getCategories();
    this.navService.changebool(this.displayNav);
  }

  async getCategories(){
    this.categoryService.getAllCat().subscribe(cat =>{
      this.result=cat;
      this.categories = this.result.data;
    });
  }
}
