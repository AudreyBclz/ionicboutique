import { Component, OnInit } from '@angular/core';
import { Order } from 'src/app/model/order';
import { OrderLine } from 'src/app/model/order-line';
import { NavService } from 'src/app/service/nav.service';
import { OrderService } from '../service/order.service';

@Component({
  selector: 'app-history-order',
  templateUrl: './history-order.page.html',
  styleUrls: ['./history-order.page.scss'],
})
export class HistoryOrderPage implements OnInit {

  subTotal =0;
  result: any;
  idUser = JSON.parse(localStorage.getItem('isLog')).id;
  displayNav = true;
  orders: Order[];
  orderLines=[];
  orderLines2: any=[];
  constructor(
    private navService: NavService,
    private orderService: OrderService
  ) { }

  ngOnInit() {
    this.navService.changebool(this.displayNav);
    this.findOrders(this.idUser);
  }
  findOrders(idUser: number){
    this.orderService.findOrdersByIdUser(idUser).subscribe(res =>{
      this.result = res;
      this.orders = this.result.data;
      console.log(this.orders);
      this.orders.forEach(order => {
        this.findOrderLines(order.id);
        console.log(this.orderLines);

      });
    });
  }
   findOrderLines(idOrder: number){
    this.orderService.findOrderLinesByIdOrder(idOrder).subscribe(res =>{
      this.result = res;
      this.orderLines = [...this.orderLines,...this.result.data];
      console.log(this.orderLines);
      return this.orderLines;
    });
    return this.orderLines;
  }

}
