import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';
import { SwiperModule } from 'swiper/angular';
import { ProductPageRoutingModule } from './product-routing.module';
import { HttpClientModule } from '@angular/common/http';
import { ProductPage } from './product.page';
import { CategoryService } from './service/category.service';
import { ProductService } from './service/product.service';
import { DetailProductPageRoutingModule } from './detail-product/detail-product-routing.module';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    ProductPageRoutingModule,
    HttpClientModule,
    SwiperModule,
    DetailProductPageRoutingModule
  ],
  declarations: [ProductPage],
  providers:[CategoryService,ProductService]
})
export class ProductPageModule {}
