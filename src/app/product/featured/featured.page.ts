import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from 'src/app/model/product';
import { ProductService } from '../service/product.service';

@Component({
  selector: 'app-featured',
  templateUrl: './featured.page.html',
  styleUrls: ['./featured.page.scss'],
})
export class FeaturedPage implements OnInit {

  result: any;
  products: Product[];
  constructor(
    private router: Router,
    private productService: ProductService
  ) { }

  ngOnInit() {
    this.getFeaturedProd();
  }


  goDetail(id: number){
    this.router.navigate(['product','detail',id]);
  }

  async getFeaturedProd(){
    this.productService.findfeaturedProd().subscribe(prod =>{
      this.result = prod;
      this.products = this.result.data;

    });
  }
}
