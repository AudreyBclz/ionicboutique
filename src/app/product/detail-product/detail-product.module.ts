import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { DetailProductPageRoutingModule } from './detail-product-routing.module';
import { SwiperModule } from 'swiper/angular';
import { DetailProductPage } from './detail-product.page';
import { ProductService } from '../service/product.service';
import { NotePipe } from 'src/app/pipe/note.pipe';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    DetailProductPageRoutingModule,
    SwiperModule,
    ReactiveFormsModule
  ],
  declarations: [DetailProductPage,NotePipe],
  providers:[ProductService]
})
export class DetailProductPageModule {}
