/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit, ViewEncapsulation } from '@angular/core';
import { ActivatedRoute, Router } from '@angular/router';
import { Product } from 'src/app/model/product';
import { ProductService } from '../service/product.service';
import SwiperCore, { Pagination } from 'swiper';
import { StorageService } from '../service/storage.service';
import { NavService } from 'src/app/service/nav.service';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Cart } from 'src/app/model/cart';
import { ToastController } from '@ionic/angular';
import { v1 as uuidv1} from 'uuid';

SwiperCore.use([Pagination]);

@Component({
  selector: 'app-detail-product',
  templateUrl: './detail-product.page.html',
  styleUrls: ['./detail-product.page.scss'],
  encapsulation:ViewEncapsulation.None
})
export class DetailProductPage implements OnInit {

  idUser: number;
  tabCart: Cart[];
  form: FormGroup;
  displayNav=true;
  isFav: boolean;
  isCart: boolean;
  tabFavori;
  tabColorOrTaille=[];
  product: Product;
  result: any;
  id: number;
  constructor(
    private router: Router,
    private activeRoute: ActivatedRoute,
    private productService: ProductService,
    private storageService: StorageService,
    private navService: NavService,
    private fb: FormBuilder,
    private toastController: ToastController) { }

 async ngOnInit() {
  this.form = this.fb.group({
    size:['',Validators.required],
    color:['',Validators.required]
  });

    this.id = this.activeRoute.snapshot.params.id;
    await this.findAProduct(this.id);
    await this.storageService.init();
    await this.initFavori();
    await this.initCart();
    this.checkFav(this.id);
    this.navService.changebool(this.displayNav);
    this.idUser=JSON.parse(localStorage.getItem('isLog')).id;
  }

  get size(){
    return this.form.get('size');
  }
  get color(){
    return this.form.get('color');
  }

  // async ionViewWillEnter(){
  //   this.id = this.activeRoute.snapshot.params.id;
  //   await this.initFavori();
  //   this.checkFav(this.id);
  // }

  // fonction pour trouver un  un produit
  async findAProduct(id: number){
     this.productService.findAProduct(id).subscribe(res =>{
      this.result = res;
      this.product= this.result.data;

    });
  }
  // afficher la couleur ou la taille
  displayColOrTaille(){
    const segButtons = document.querySelectorAll('ion-segment-button');
    if(segButtons[0].getAttribute('aria-selected')==='true'){
      this.tabColorOrTaille = this.product.tailles.split(',');
    }else{
      this.tabColorOrTaille = this.product.colors.split(',');
    }
  }
  //fonction pour initialiser
  async initFavori(){
    const promise= await this.storageService.get('favorite');
    console.log(promise);

    if(promise!==null){
      this.storageService.get('favorite').then(fav =>{
        this.tabFavori= fav;
      });
    }else{
      this.tabFavori=[];
    }
    return this.tabFavori;
  }


  async addFavori(id: number){
    this.isFav= true;
    let isHere= false;

    if(this.tabFavori.length===0){
      this.tabFavori.push(id);
      this.storageService.set('favorite',this.tabFavori);

    }
    else{
      this.tabFavori.forEach(fav => {
        if(fav === id){
          console.log( id,'si id déjà présent');
          this.isFav=true;
          isHere= true;
          return;
        }
      });
        if(!isHere){
          this.tabFavori.push(id);
          this.storageService.set('favorite',this.tabFavori);
        }
      };
    }

    // fonction pour enlever un favori
    async removeFavori(id: number){
      let tab2: number[] = this.tabFavori;
      tab2=this.tabFavori.filter((i: number) =>i!==id);
      this.storageService.set('favorite',tab2);
      console.log(tab2);
      await this.initFavori();

    }

    // fonction pour si le produit est dans les favoris
    checkFav(id: number){
      this.isFav=false;
      if(this.tabFavori !== undefined){
        this.tabFavori.forEach((fav: number) => {
          if (fav === id) {
            this.isFav =true;
          }
          return this.isFav;
        });
      }
  }


  async initCart(){
    const promise= await this.storageService.get('cart');

    if(promise!==null){
      this.storageService.get('cart').then(cart =>{
        this.tabCart= cart;
      });
    }else{
      this.tabCart=[];
    }
    return this.tabCart;
  }

  // fonction pour ajouter un élément au panier
  async addToCart(id: number){
    let msg='';
    if(this.form.valid){
      const obj: Cart = {
        id:uuidv1(),
        idUser:this.idUser,
        idProduct:this.id,
        taille:this.size.value,
        color:this.color.value,
        quantity:1,
        price:this.product.actualPrice
      };

    this.isCart= true;
    let isHere= false;
    console.log('début fonction',this.tabCart);

    if(this.tabCart.length===0){
      this.tabCart.push(obj);
      this.storageService.set('cart',this.tabCart);
      console.log( 'boucle du 1er si',this.tabCart);
      msg='L\'article a bien été ajouté au panier';
    }
    else{
      this.tabCart.forEach(prod => {
        if(prod.idProduct === id){
          console.log( id,'si id déjà présent');
          this.isCart=true;
          isHere= true;
          msg='Vous avez déjà ajouter cet article au panier';
          return;
        }
      });
        if(!isHere){
          this.tabCart.push(obj);
          this.storageService.set('cart',this.tabCart);
          console.log( id,'id pas présent',this.tabCart);
          msg='L\'article a bien été ajouté au panier';

        }
      ;};
      const toast = await this.toastController.create({
        message: msg,
        duration: 1500,
        position: 'top'
      });
      await toast.present();
    }

    }
    buy(id: number){
      this.addToCart(id);
      this.router.navigate(['','product','order']);
    }
}

