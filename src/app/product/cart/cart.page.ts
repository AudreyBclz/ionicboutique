/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { ToastController } from '@ionic/angular';
import { BehaviorSubject } from 'rxjs';
import { Cart } from 'src/app/model/cart';
import { Product } from 'src/app/model/product';
import { NavService } from 'src/app/service/nav.service';
import { ProductService } from '../service/product.service';
import { StorageService } from '../service/storage.service';

@Component({
  selector: 'app-cart',
  templateUrl: './cart.page.html',
  styleUrls: ['./cart.page.scss'],
})
export class CartPage implements OnInit {

  // private _tabProduct2= new BehaviorSubject<Product[]>(null);
  tabCart: Cart[];
  displayNav=true;
  result: any;
  product: Product;
  tabProduct: Product[];
  constructor(
    private navside: NavService,
    private productService: ProductService,
    private storageService: StorageService,
    private router: Router,
    private toastController: ToastController
  ) { }

    //  get tabProduct2(){
    //    // eslint-disable-next-line no-underscore-dangle
    //    return this._tabProduct2.getValue()? this._tabProduct2.asObservable():this.getProduct2();
    //  }

    // getProduct2(){
    //   this.tabProduct=[];
    // this.storageService.get('cart').then(cart =>{
    //     this.tabCart = cart;
    //  }).then(() =>{
    //   this.tabCart.forEach(cartItem =>{
    //     this.findAProduct(cartItem.idProduct);
    //     console.log(this.tabProduct);
    //   });
    //  });
    //  return this.tabProduct;
    // }

  async ngOnInit() {
    this.navside.changebool(this.displayNav);
    await this.storageService.init();
    this.tabProduct=[];
  }
  async ionViewWillEnter(){
    this.tabProduct=[];
    this.storageService.get('cart').then(cart =>{
        this.tabCart = cart;
     }).then(() =>{
      this.tabCart.forEach(cartItem =>{
        this.findAProduct(cartItem.idProduct);
        console.log(this.tabProduct);
      });
     });
  }

  // fonction pour trouver les produits et les ajouter dans le tableau de produit
  async findAProduct(id: number){
    this.productService.findAProduct(id).subscribe({next: (res) =>{
     this.result = res;
     this.product= this.result.data;
     this.tabProduct = [...this.tabProduct,this.product];
     console.log(this.tabProduct);

    }
   });
 }

  async clearAprod(id: number){
  this.tabCart.splice(id,1);
  this.storageService.set('cart',this.tabCart);
  this.storageService.get('cart').then(cart =>{
    this.tabCart = cart;
 }).then(() =>{
  this.tabProduct=[];
   this.tabCart.forEach(async cartItem =>{
      await this.findAProduct(cartItem.idProduct);
    console.log(this.tabProduct);
  });
 });
 const toast = await this.toastController.create({
  message: 'Le produit a bien été supprimé de votre panier',
  duration: 1500,
  position: 'top'
  });
  await toast.present();
  this.router.navigate(['product','detail',this.product.id]);
 }

 removeAQuantity(i: number){
  const quant = this.tabCart[i].quantity;
  if(quant>1){
    this.tabCart[i].quantity = this.tabCart[i].quantity-1;
  }else if(quant===1){
    this.clearAprod(i);
  }
  console.log(this.tabCart);

  this.storageService.set('cart',this.tabCart);
 }
 addAQuantity(i: number){
    this.tabCart[i].quantity = this.tabCart[i].quantity+1;
    this.storageService.set('cart',this.tabCart);
    console.log(this.tabCart);
 }
}
