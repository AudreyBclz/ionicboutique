import { Component, OnInit } from '@angular/core';
import { NavService } from '../service/nav.service';

@Component({
  selector: 'app-home',
  templateUrl: 'home.page.html',
  styleUrls: ['home.page.scss'],
})
export class HomePage implements OnInit {

  displayNav=false;
  constructor(private navService: NavService) {}

  ngOnInit(): void {
    this.navService.changebool(this.displayNav);
  }

}
