import { HttpClient } from '@angular/common/http';
import { Injectable } from '@angular/core';
import { User } from '../model/user';

@Injectable({
  providedIn: 'root'
})
export class UserService {

  url = 'http://localhost:4000/api/boutique';
  constructor(private http: HttpClient) { }

  inscription(user: User){
    return this.http.post<User>(this.url+'/inscription',user);
  }
  connexion(data: any){
    return this.http.post(this.url+'/connexion',data);
  }
  findAUser(id: number){
    return this.http.get<User>(this.url+'/user/'+id);
  }
}
