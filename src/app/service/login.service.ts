/* eslint-disable object-shorthand */
import { Injectable } from '@angular/core';
import { Router } from '@angular/router';

@Injectable({
  providedIn: 'root'
})
export class LoginService {

 isLogin = false;
  constructor(private router: Router) { }

  connect(id: number){
    if(JSON.parse(localStorage.getItem('isLog')) == null || undefined){
      this.isLogin = true;
      localStorage.setItem('isLog',JSON.stringify(
        {
          isLog:this.isLogin,
          id:id,
        }));
    }
  }
  isLog(): boolean{
    if(JSON.parse(localStorage.getItem('isLog'))!= null){
      this.isLogin = JSON.parse(localStorage.getItem('isLog')).isLog;
    }
    else{
      this.isLogin=false;
    }
    return this.isLogin;
  }

  deconnect() {
    localStorage.removeItem('isLog');
    this.router.navigate(['/connexion']);
  }
}

