/* eslint-disable @typescript-eslint/member-ordering */

import { Injectable, Output, EventEmitter } from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class NavService {

  displayBool: boolean;
  @Output()
  display: EventEmitter<boolean>= new EventEmitter();
  constructor() {
   }
   changebool(dis: boolean){
    this.display.emit(dis);
   }
}
