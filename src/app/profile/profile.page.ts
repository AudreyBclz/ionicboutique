import { Component, OnInit } from '@angular/core';
import { User } from '../model/user';
import { NavService } from '../service/nav.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-profile',
  templateUrl: './profile.page.html',
  styleUrls: ['./profile.page.scss'],
})
export class ProfilePage implements OnInit {

  displayNav=true;
  result: any;
  user: User;
  id=JSON.parse(localStorage.getItem('isLog')).id;
  constructor(
    private userService: UserService,
    private navService: NavService) { }

  async ngOnInit() {
   await this.findAUser(this.id);
   this.navService.changebool(this.displayNav);
  }

  async findAUser(id: number){
    this.userService.findAUser(id).subscribe(user =>{
      this.result = user;
      this.user = this.result.data;
    });
  }

}
