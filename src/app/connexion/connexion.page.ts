/* eslint-disable prefer-arrow/prefer-arrow-functions */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { LoginService } from '../service/login.service';
import { NavService } from '../service/nav.service';
import { UserService } from '../service/user.service';

@Component({
  selector: 'app-connexion',
  templateUrl: './connexion.page.html',
  styleUrls: ['./connexion.page.scss'],
})
export class ConnexionPage implements OnInit {

  displayNav=false;
  result: any;
  alerte: string;
  bool=false;
  typePass='password';
  form: FormGroup;
  constructor(
    private userService: UserService,
    private fb: FormBuilder,
    private router: Router,
    private loginService: LoginService,
    private navService: NavService) { }

  ngOnInit() {
    this.form= this.fb.group({
      email:['',Validators.required],
      password:['',Validators.required],
    });
    this.navService.changebool(this.displayNav);
  }
  get email(){
    return this.form.get('email');
  }
  get password(){
    return this.form.get('password');
  }

  connecter(){
    if(this.form.valid){
      const user ={
        email:this.email.value,
        password: this.password.value
      };
      this.userService.connexion(user).subscribe(res => {
        this.result=res;
        this.loginService.connect(this.result.data.id);
        this.router.navigate(['product']);
      },
      err =>{
        console.log(err);
        this.alerte = err.error.message;
        setTimeout(()=>{
          this.alerte='';
        },3000);
      }
    );
  }
}

  togglePass(){
    if(this.typePass === 'password'){
      this.typePass='text';
      this.bool=false;
    }else{
      this.typePass='password';
      this.bool=true;
    }
  }
}
