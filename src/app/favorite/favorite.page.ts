import { Component, OnInit } from '@angular/core';
import { Router } from '@angular/router';
import { Product } from '../model/product';
import { ProductService } from '../product/service/product.service';
import { StorageService } from '../product/service/storage.service';
import { NavService } from '../service/nav.service';

@Component({
  selector: 'app-favorite',
  templateUrl: './favorite.page.html',
  styleUrls: ['./favorite.page.scss'],
})
export class FavoritePage implements OnInit {

  displayNav=true;
  result: any;
  tabProduct: Product[]=[];
  tabFavori: number[]|[];
  product: Product;
  constructor(
    private storageService: StorageService,
    private productService: ProductService,
    private router: Router,
    private navService: NavService) { }

  async ngOnInit() {
    console.log('ini');
    this.navService.changebool(this.displayNav);

    await this.storageService.init();
      this.tabProduct=[];
    }

    ionViewWillEnter(): void {
      this.tabProduct=[];
        this.storageService.get('favorite').then(fav =>{
            this.tabFavori = fav;
         }).then(() =>{
          if(this.tabFavori!==null){
            this.tabFavori.forEach(id =>{
              this.findAProduct(id);
            });
          }
         });

      }
// fonction pour trouver les produits et les ajouter dans le tableau de produit
  async findAProduct(id: number){
    this.productService.findAProduct(id).subscribe({next: (res) =>{
     this.result = res;
     this.product= this.result.data;
     this.tabProduct = [...this.tabProduct,this.product];
    }
   });
 }

 goDetail(id: number){
  this.router.navigate(['product','detail',id]);
 }
}
