import { Component, OnInit } from '@angular/core';
import { LoginService } from './service/login.service';
import { NavService } from './service/nav.service';

@Component({
  selector: 'app-root',
  templateUrl: 'app.component.html',
  styleUrls: ['app.component.scss'],
})
export class AppComponent implements OnInit {
  display =true;
  constructor(
    private loginservice: LoginService,
    private navService: NavService) {}

  ionViewWillEnter(){
    if(localStorage.getItem('isLog')!== undefined||null){
      this.display=false;
    }else{
      this.display=true;
    }
  }
  onLogOut(){
    this.loginservice.deconnect();
  }
  ngOnInit(){
    this.navService.display.subscribe(x => this.display = x);
  }
}
