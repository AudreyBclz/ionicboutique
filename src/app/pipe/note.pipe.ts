/* eslint-disable @typescript-eslint/quotes */
import { Pipe, PipeTransform } from '@angular/core';

@Pipe({
  name: 'note'
})
export class NotePipe implements PipeTransform {

  transform(value: number): string{
   let comment: string;
   switch (true) {
    case value<1:
      comment="Poor";
      break;
    case value<2:
      comment="Bad";
      break;
    case value<3:
      comment="Good";
      break;
    case value<4:
      comment="Very good";
      break;
    case value<=5:
      comment="Perfect";
      break;
   }
   return comment;
  }

}
