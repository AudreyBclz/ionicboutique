/* eslint-disable no-trailing-spaces */
/* eslint-disable @typescript-eslint/member-ordering */
import { Component, OnInit } from '@angular/core';
import { FormBuilder, FormGroup, Validators } from '@angular/forms';
import { Router } from '@angular/router';
import { PopoverController } from '@ionic/angular';
import { User } from '../model/user';
import { NavService } from '../service/nav.service';
import { UserService } from '../service/user.service';
import { PopoverComponent } from './popover/popover.component';

@Component({
  selector: 'app-inscription',
  templateUrl: './inscription.page.html',
  styleUrls: ['./inscription.page.scss'],
})
export class InscriptionPage implements OnInit {

  displayNav=false;
  sexe: string;
  bool=true;
  typePass='password';
  form: FormGroup;
  constructor(
    private fb: FormBuilder,
    private popoverController: PopoverController,
    private userService: UserService,
    private router: Router,
    private navService: NavService
  ) { }

  ngOnInit() {
    this.form = this.fb.group({
      name:['',Validators.required],
      email:['',[Validators.required,Validators.pattern('^([a-z]*)@([a-z]{2,10})(.fr|.com)$')]],
      password:['',[Validators.required,Validators.pattern('^(?=.*?[A-Z])(?=.?[a-z])(?=.*?[0-9])(?=.*?[#?!@$%^&*-]).{8,}$')]],
      phone:['',[Validators.required,Validators.pattern('^[0]{1}[1-9]{1}[0-9]{8}$')]],
      address:['',Validators.required],
      city:['',Validators.required],
    });
    this.navService.changebool(this.displayNav);
  }

  get name(){
    return this.form.get('name');
  }
  get email(){
    return this.form.get('email');
  }
  get password(){
    return this.form.get('password');
  }
  get phone(){
    return this.form.get('phone');
  }
  get address(){
    return this.form.get('address');
  }
  get city(){
    return this.form.get('city');
  }

  inscrire(){
    if(this.form.valid){
      console.log('coucou');
      const sexe= document.querySelector('ion-radio-group').value;
      const user= new User();
      user.name=this.name.value;
      user.email=this.email.value;
      user.password=this.password.value;
      user.phone=this.phone.value;
      user.sexe=sexe;
      user.adresse=this.address.value;
      user.city=this.city.value;

      this.userService.inscription(user).subscribe( x =>{
        this.router.navigate(['connexion']);
      });
    }
  }

  togglePass(){
    if(this.typePass === 'password'){
      this.typePass='text';
      this.bool=false;
    }else{
      this.typePass='password';
      this.bool=true;
    }
  }
  async presentPopover(e: Event){
    const popover = await this.popoverController.create({
      component:PopoverComponent,
      event: e
    });
    await popover.present();

    const {role} = await popover.onDidDismiss();
  }


}
