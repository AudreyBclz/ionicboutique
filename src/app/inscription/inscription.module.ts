import { NgModule } from '@angular/core';
import { CommonModule } from '@angular/common';
import { FormsModule, ReactiveFormsModule } from '@angular/forms';

import { IonicModule } from '@ionic/angular';

import { InscriptionPageRoutingModule } from './inscription-routing.module';

import { InscriptionPage } from './inscription.page';
import { PopoverComponent } from './popover/popover.component';
import { UserService } from '../service/user.service';

@NgModule({
  imports: [
    CommonModule,
    FormsModule,
    IonicModule,
    InscriptionPageRoutingModule,
    ReactiveFormsModule
  ],
  declarations: [InscriptionPage,PopoverComponent],
  providers:[UserService]
})
export class InscriptionPageModule {}
