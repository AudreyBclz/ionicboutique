export interface OrderLine {
  id?: number;
  nameProduct: string;
  quantity: number;
  taille: string;
  color: string;
  priceUnit: number;
  idOrder: number;
}
