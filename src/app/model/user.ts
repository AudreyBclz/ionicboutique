export class User {
  id?: number;
  name: string;
  email: string;
  password: string;
  phone: string;
  sexe: string;
  adresse: string;
  city: string;
}
