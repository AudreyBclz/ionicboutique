export interface Cart {
  id: string;
  idUser: number;
  idProduct: number;
  taille: string;
  color: string;
  quantity: number;
  price: number;
}
