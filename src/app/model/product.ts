export interface Product {
  id?: number;
  name: string;
  pictures: string;
  actualPrice: number;
  lastPrice: number;
  note: number;
  nbVue: number;
  description: string;
  idCategory: number;
  colors: string;
  tailles: string;
  feature: boolean;
  bestSell: boolean;
}
