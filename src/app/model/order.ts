export interface Order {
  id?: number;
  numCommande?: string;
  date?: Date;
  idUser: number;
}
