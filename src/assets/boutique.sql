-- phpMyAdmin SQL Dump
-- version 5.2.0
-- https://www.phpmyadmin.net/
--
-- Hôte : 127.0.0.1
-- Généré le : dim. 23 oct. 2022 à 16:33
-- Version du serveur : 10.4.24-MariaDB
-- Version de PHP : 8.1.6

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
START TRANSACTION;
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- Base de données : `boutique`
--

-- --------------------------------------------------------

--
-- Structure de la table `categories`
--

CREATE TABLE `categories` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `image` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `categories`
--

INSERT INTO `categories` (`id`, `name`, `image`) VALUES
(1, 'Femme', '../assets/category-1.png'),
(2, 'Homme', 'https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/category-2.png'),
(3, 'Enfant', 'https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/category-3.png');

-- --------------------------------------------------------

--
-- Structure de la table `orderlines`
--

CREATE TABLE `orderlines` (
  `id` int(11) NOT NULL,
  `nameProduct` varchar(255) NOT NULL,
  `quantity` int(11) NOT NULL,
  `taille` varchar(255) NOT NULL,
  `color` varchar(255) NOT NULL,
  `priceUnit` float NOT NULL,
  `idOrder` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `orderlines`
--

INSERT INTO `orderlines` (`id`, `nameProduct`, `quantity`, `taille`, `color`, `priceUnit`, `idOrder`) VALUES
(1, 'Chemise en Jeans', 1, 'M', 'Blue', 15, 1),
(2, 'Chemise en Jeans', 1, 'L', 'Yellow', 15, 1),
(3, 'Veste en Jeans', 1, 'XL', 'Blue', 20, 2),
(9, 'Chemise en Jeans', 2, 'XXXL', 'blue', 15, 14),
(10, 'Chemise à rayure', 1, 'S', 'White', 35, 14),
(11, 'Veste en Jeans', 1, 'XS', 'Blue', 20, 18);

-- --------------------------------------------------------

--
-- Structure de la table `orders`
--

CREATE TABLE `orders` (
  `id` int(11) NOT NULL,
  `numCommande` varchar(255) NOT NULL,
  `date` datetime NOT NULL,
  `idUser` int(11) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `orders`
--

INSERT INTO `orders` (`id`, `numCommande`, `date`, `idUser`) VALUES
(1, 'e6d6d3cb-ed4a-40b9-b6e4-b15803060c99', '2022-05-09 22:00:00', 1),
(2, 'bc43b4bb-0f3d-4a71-ab6c-f68758655abc', '2022-10-06 07:50:59', 3),
(14, '1e2ac581-4d53-434d-9dd9-2a2979d71cbb', '2022-10-22 20:03:51', 4),
(18, 'fdc7b468-721b-4da8-aeb8-1fbcef768585', '2022-10-23 12:54:31', 4);

-- --------------------------------------------------------

--
-- Structure de la table `products`
--

CREATE TABLE `products` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `pictures` varchar(255) NOT NULL,
  `actualPrice` float NOT NULL,
  `lastPrice` float NOT NULL,
  `note` int(11) DEFAULT NULL,
  `nbVue` int(11) DEFAULT NULL,
  `description` text NOT NULL,
  `idCategory` int(11) NOT NULL,
  `colors` varchar(255) NOT NULL,
  `tailles` varchar(255) NOT NULL,
  `feature` tinyint(1) NOT NULL,
  `bestSell` tinyint(1) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `products`
--

INSERT INTO `products` (`id`, `name`, `pictures`, `actualPrice`, `lastPrice`, `note`, `nbVue`, `description`, `idCategory`, `colors`, `tailles`, `feature`, `bestSell`) VALUES
(1, 'Chemise à rayure', 'https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-1.png,https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-2.png,https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-3.png', 35, 40, 3, 0, 'Une jolie chemise idéale pour vos journées d\'été', 1, 'White', 'S,M,L,XL,XXL', 1, 0),
(2, 'Chemise en Jeans', 'https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-2.png,https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-2.png,https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-2.png', 15, 15, 5, 15, 'Une chemise chic et branchée pour une tenue décontractée', 2, 'blue', 'S,M,L,XL,XXL,XXXL', 0, 1),
(3, 'Veste en Jeans', 'https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-3.png,https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-3.png,https://target-mohamed-s3.s3.eu-west-3.amazonaws.com/prod-3.png', 20, 30, 4, 56, 'Veste légère idéale pour le printemps', 1, 'Blue', 'XS,S,M,L,XL', 1, 1);

-- --------------------------------------------------------

--
-- Structure de la table `users`
--

CREATE TABLE `users` (
  `id` int(11) NOT NULL,
  `name` varchar(255) NOT NULL,
  `email` varchar(255) NOT NULL,
  `password` varchar(255) NOT NULL,
  `phone` varchar(255) NOT NULL,
  `sexe` varchar(255) NOT NULL,
  `adresse` varchar(255) NOT NULL,
  `city` varchar(255) NOT NULL
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4;

--
-- Déchargement des données de la table `users`
--

INSERT INTO `users` (`id`, `name`, `email`, `password`, `phone`, `sexe`, `adresse`, `city`) VALUES
(1, 'Audrey Bu', 'audrey@audrey.fr', '$2b$10$1nLcqRqQnwK.qeiWS/5MVOLxDOb1iEE/lpzGiwIy2N9KyO0uOWK7q', '0606060606', 'Femme', '58 rue des Fleurs', 'Lille'),
(3, 'Jean Dupont', 'jean@jean.com', '$2b$10$YIji4O3QXvtgrOUkCylgeeQsyerOYO9aqj89g6MJqLYM8Uzq3FCAC', '0606060606', 'Homme', '58 rue des Fleurs', 'Lille'),
(4, 'Albert Martin', 'albert@alb.fr', '$2b$10$bhuOiUbYoL2QwyDPX5exD.IuSIGCSfv.QiOl/w/jBRfBS.mr74qti', '0102030405', 'Homme', '356 rue des Lilas 45000', 'Orléans');

--
-- Index pour les tables déchargées
--

--
-- Index pour la table `categories`
--
ALTER TABLE `categories`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `orderlines`
--
ALTER TABLE `orderlines`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `orders`
--
ALTER TABLE `orders`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `products`
--
ALTER TABLE `products`
  ADD PRIMARY KEY (`id`);

--
-- Index pour la table `users`
--
ALTER TABLE `users`
  ADD PRIMARY KEY (`id`),
  ADD UNIQUE KEY `Users_email_unique` (`email`);

--
-- AUTO_INCREMENT pour les tables déchargées
--

--
-- AUTO_INCREMENT pour la table `categories`
--
ALTER TABLE `categories`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `orderlines`
--
ALTER TABLE `orderlines`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=12;

--
-- AUTO_INCREMENT pour la table `orders`
--
ALTER TABLE `orders`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=19;

--
-- AUTO_INCREMENT pour la table `products`
--
ALTER TABLE `products`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=4;

--
-- AUTO_INCREMENT pour la table `users`
--
ALTER TABLE `users`
  MODIFY `id` int(11) NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=5;
COMMIT;

/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
